from selenium import webdriver
import os

expected_title = 'Demobank - Bankowość Internetowa - Logowanie'

if os.name == 'nt':
    driver = webdriver.Chrome(executable_path='chromedriver.exe')
else:
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(options=chrome_options)

driver.get('https://demobank.jaktestowac.pl/logowanie_etap_1.html')
driver.get_screenshot_as_file("screenshot.png")
observed_title = driver.title

assert expected_title == observed_title, 'Title not match'
driver.quit()
